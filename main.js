const v8n = require("v8n");
v8n.extend({
  myCustomRule: expected => {
    return value => value === expected;
  }
});

{
  console.log("> test");
  const v8nModel = v8n()
    .string()
    .first("A")
    .last("e")
    .myCustomRule("Apple");

  {
    const result = v8nModel.test("Apple");
    console.log(result);
  }

  {
    const result = v8nModel.test("Banana");
    console.log(result);
  }
}

{
  console.log("> testAll");
  const v8nModel = v8n()
    .string()
    .first("A")
    .last("e")
    .myCustomRule("Apple");

  {
    const errors = v8nModel.testAll("Apple");
    console.log(errors.map(error => error.rule.name));
  }

  {
    const errors = v8nModel.testAll("Banana");
    console.log(errors.map(error => error.rule.name));
  }
}

{
  console.log("> check");
  const v8nModel = v8n()
    .string()
    .first("A")
    .last("e")
    .myCustomRule("Apple");

  try {
    const result = v8nModel.check("Apple");
    console.log(result);
  } catch (error) {
    console.log(error.rule.name);
  }

  try {
    const result = v8nModel.check("Banana");
    console.log(result);
  } catch (error) {
    console.log(error.rule.name);
  }
}
